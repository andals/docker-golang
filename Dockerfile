FROM andals/centos:7.2.1511
MAINTAINER tabalt <tabalt@tabalt.net>

LABEL name="Golang Image"
LABEL vendor="Andals"

COPY pkg/ /build/pkg/
COPY script/ /build/script/

RUN /build/script/build_image.sh

CMD /build/script/init.sh
