#!/bin/bash

buildRoot=/build
installDstRoot=/usr/local

srcRoot=$buildRoot/src
buildTmpRoot=$buildRoot/tmp
pkgRoot=$buildRoot/pkg
scriptRoot=$buildRoot/script

$scriptRoot/pre_build.sh

cd $installDstRoot
tar zxvf $pkgRoot/go1.5.1.linux-amd64.tar.gz
ln -s go1.5.1.linux-amd64 go

